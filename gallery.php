<!-- Header -->
<?php require_once('header.php'); ?>

<body>
		<div id="topBorder"></div>

		<div id="coverContainer">
			<div id="galleryCover"></div>
		</div><!-- coverContainer -->

		<!-- navigation bar -->
		<?php require_once('navbar.php'); ?>

		<div class="mainContainer">

			<h1 class="centerblock">Hálsaból</h1>
			<h2 class="centerblock">Image Gallery</h2>

			<div class="thumbBox">
				<a class="fancybox" href="assets/images/Gallery/pic01.jpg" data-fancybox-group="gallery" title="The cabins at the base of Kirkjufell">
					<img class="thumbImg" src="assets/images/Gallery/thumbs/pic01.jpg" alt="pic01" /> </a>

					<a class="fancybox" href="assets/images/Gallery/pic02.jpg" data-fancybox-group="gallery" title="Cabins from outside">
					<img class="thumbImg" src="assets/images/Gallery/thumbs/pic02.jpg" alt="pic02" /> </a>

					<a class="fancybox" href="assets/images/Gallery/pic03.jpg" data-fancybox-group="gallery" title="Inside the bedrooms (both are identical)">
					<img class="thumbImg" src="assets/images/Gallery/thumbs/pic03.jpg" alt="pic03" /> </a>

					<a class="fancybox" href="assets/images/Gallery/pic04.jpg" data-fancybox-group="gallery" title="The bathroom">
					<img class="thumbImg" src="assets/images/Gallery/thumbs/pic04.jpg" alt="pic04" /> </a>

					<a class="fancybox" href="assets/images/Gallery/pic05.jpg" data-fancybox-group="gallery" title="The living room">
					<img class="thumbImg" src="assets/images/Gallery/thumbs/pic05.jpg" alt="pic05" /> </a>

					<a class="fancybox" href="assets/images/Gallery/pic06.jpg" data-fancybox-group="gallery" title="Living room and kitchen">
					<img class="thumbImg" src="assets/images/Gallery/thumbs/pic06.jpg" alt="pic06" /> </a>

					<a class="fancybox" href="assets/images/Gallery/pic07.jpg" data-fancybox-group="gallery" title="The kitchen">
					<img class="thumbImg" src="assets/images/Gallery/thumbs/pic07.jpg" alt="pic07" /> </a>

					<a class="fancybox" href="assets/images/Gallery/pic08.jpg" data-fancybox-group="gallery" title="View at the bedrooms from the livingroom and kitchen">
					<img class="thumbImg" src="assets/images/Gallery/thumbs/pic08.jpg" alt="pic08" /> </a>

					<a class="fancybox" href="assets/images/Gallery/pic09.jpg" data-fancybox-group="gallery" title="The kitchen">
					<img class="thumbImg" src="assets/images/Gallery/thumbs/pic09.jpg" alt="pic09" /> </a>

					<a class="fancybox" href="assets/images/Gallery/pic10.jpg" data-fancybox-group="gallery" title="The kitchen">
					<img class="thumbImg" src="assets/images/Gallery/thumbs/pic10.jpg" alt="pic10" /> </a>

					<a class="fancybox" href="assets/images/Gallery/pic11.jpg" data-fancybox-group="gallery" title="The hot tub">
					<img class="thumbImg" src="assets/images/Gallery/thumbs/pic11.jpg" alt="pic11" /> </a>

					<a class="fancybox" href="assets/images/Gallery/pic12.jpg" data-fancybox-group="gallery" title="View from the porch">
					<img class="thumbImg" src="assets/images/Gallery/thumbs/pic12.jpg" alt="pic12" /> </a>

					<a class="fancybox" href="assets/images/Gallery/pic13.jpg" data-fancybox-group="gallery" title="Cabin">
					<img class="thumbImg" src="assets/images/Gallery/thumbs/pic13.jpg" alt="pic13" /> </a>

					<a class="fancybox" href="assets/images/Gallery/pic14.jpg" data-fancybox-group="gallery" title="Playground at the base of Kirkjufell">
					<img class="thumbImg" src="assets/images/Gallery/thumbs/pic14.jpg" alt="pic14" /> </a>

					<a class="fancybox" href="assets/images/Gallery/pic15.jpg" data-fancybox-group="gallery" title="Playground and cabin">
					<img class="thumbImg" src="assets/images/Gallery/thumbs/pic15.jpg" alt="pic15" /> </a>

					<a class="fancybox" href="assets/images/Gallery/pic16.jpg" data-fancybox-group="gallery" title="One of the cabins at the base of Kirkjufell">
					<img class="thumbImg" src="assets/images/Gallery/thumbs/pic16.jpg" alt="pic16" /> </a>
			</div> <!-- thumbBox End -->
		</div> <!-- container -->

		<!-- footer -->
		<?php require_once('footer.php'); ?>
</body>
</html>