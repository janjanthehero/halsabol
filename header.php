<!DOCTYPE HTML>
<html lang="en">
<head>
	<!-- Google Analytics -->
	<?php require_once('analytics.php');?>
	
	<title><?php
			$page = basename($_SERVER['PHP_SELF']);
			switch ($page) {
				case 'index.php':
					$title = 'Hálsaból Cottages At The Base Of Kirkjufell Mountain';
					break;
				case 'gallery.php':
					$title = 'Halsabol - Image Gallery';
					break;
				case 'contact.php':
					$title = 'Contact or Book Hálsaból vacation cottages';
					break;
				default:
					$title = 'Hálsaból';
					break;
				}
				echo($title);
		?></title>

<?php
	$page = basename($_SERVER['PHP_SELF']);
	
	if($page == 'index.php' || $page == 'gallery.php'){
		require_once('galleryScripts.php'); // Load gallery related scripts only on Index and Gallery pages
	}
?>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Cosy cottage rental at the base of mount Kirkjufell near the town of Grundarfjordur, Iceland, surrounded by breathtaking nature and scenery."/>
	
	<!-- Bootstrap -->
	<link rel="stylesheet" id="bootstrap-css" href="css/bootstrap.min.css" type="text/css">

	<!-- Favicon -->
	<link rel="shortcut icon" href="assets/images/icons/favicon.ico">

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css2?family=Cairo&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans+Condensed:wght@300&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Barlow:wght@200&display=swap" rel="stylesheet">

	<!-- Main Styles -->
	<link rel="stylesheet" href="css/style.css" type="text/css">
	<link rel="stylesheet" href="css/style-navbar.css" type="text/css">
	<link rel="stylesheet" href="css/style-footer.css" type="text/css">
	<link rel="stylesheet" href="css/style-contact-form.css" type="text/css">
	<link rel="stylesheet" href="css/style-media-1312.css" type="text/css">
	<link rel="stylesheet" href="css/style-media-992.css" type="text/css">
	<link rel="stylesheet" href="css/style-media-483.css" type="text/css">
	<!-- Main Styles End -->
</head>
