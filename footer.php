<footer>
	<div class="footerWrapper">
		<div class="footerSpan">
			<a id="footerIcon" href="https://www.facebook.com/H%C3%A1lsab%C3%B3l-237766746363340/"><img src="assets/images/icons/fb-icon.png" class="social-icon" alt="facebook social media icon"></a>
		</div>
		<div class="footerSpan">
			<a href="index.php">Home</a><p> |  </p>
			<a href="gallery.php">Gallery</a><p> |  </p>
			<a href="contact.php">Contact/Book</a>
		</div>
		<div class="footerSpan">
			<p>All rights reserved ©<?php echo date('Y') ?> <a href="https://www.linkedin.com/in/janis-karimovs-82a085142/" style="font-weight: unset; margin-right: 0;">Janis Karimovs</a></p>
		</div>
	</div>
</footer>
