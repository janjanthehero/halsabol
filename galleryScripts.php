    <script src="https://code.jquery.com/jquery-latest.min.js"></script>

    <link rel="stylesheet" href="fbCss/jquery.fancybox.css?v=2.1.4" type="text/css" media="screen" />
    <script src="js/jquery.fancybox.pack.js?v=2.1.4"></script>

    <link rel="stylesheet" href="helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
    <script src="helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
    <script src="helpers/jquery.fancybox-media.js?v=1.0.5"></script>

    <link rel="stylesheet" href="helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
    <script src="helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

    <script>
        $(document).ready(function() {
            $(".fancybox").fancybox();
        });
    </script>
    