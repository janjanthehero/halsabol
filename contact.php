<!-- Header -->
<?php require_once('header.php'); ?>

<body>
		<div id="topBorder"></div>

		<div id="coverContainer">
			<div id="contactCover"></div>
		</div>

		<!-- Navigation Bar -->
		<?php require_once('navbar.php'); ?>

		<!-- Inline style expands the google maps window to the edges of container.
		Try to fix the issue properly instead of using this hack. -->
		<div class="mainContainer" style="padding-left:0; padding-right: 0;">

			<h1 class="centerblock">Hálsaból</h1>

			<h2 class="centerText">Questions? Want to book?</h2>

			<p class="centerText">Get in touch with us using the form below. <br>
									And make sure to include your correct email address so we can get back to you.</p>

			<hr class="dottedHr">

			<div id="formBox">
				<form id="contactForm" class="form-horizontal" role="form">
					<input type="text" class="form-control" id="contact-name" placeholder="Name" name="name">
					<input type="email" class="form-control" id="contact-email" placeholder="*Email" name="email" required>
					<input type="text" class="form-control" id="contact-subject" placeholder="Subject" name="subject">
					<textarea class="form-control" id="contact-textarea" placeholder="*Message" name="message" required></textarea>
					<button type="submit" id="form-submit" class="btn btn-primary btn-lg" name="submit">Send</button>
					<div id="response"></div>
				</form>
			</div>

			<hr class="dottedHr">

			<div class="contactBox centerblock">
				<div class="halfDivLeft">
					<p class="rightText"><b>Phone</b></p>
					<p class="rightText"><b>Email</b></p>
					<p class="rightText"><b>Address</b></p>
				</div>

				<div class="halfDivRight">
					<p class="leftText">+354 8476606</p>
					<p class="leftText">halsabol@halsabol.is</p>
					<p class="leftText">Snæfellsnesvegur, Háls, 350 Grundarfjörður, Iceland</p>
				</div>
			</div>

			<!-- Google Maps -->
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d9564.025814470124!2d-23.29749097225737!3d64.92825172497615!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48d57babbc8aee93%3A0x381bf9981b26b629!2zSMOhbHNhYsOzbCBTdW1hcmjDunM!5e0!3m2!1sen!2sdk!4v1487429338249"
				width="100%" height="500" frameborder="0" style="border:0; margin-top: 2em; margin-bottom: -1.5em;" allowfullscreen></iframe>
		</div> <!-- container End -->

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> <!-- Form jQuery Script -->
    	<script src="js/bootstrap.min.js"></script> <!-- Form Bootstrap Script -->
    	<script src="js/form-submit.js"></script> <!-- Form Submit Script -->

		<!-- Footer -->
		<?php require_once('footer.php'); ?>
</body>
</html>