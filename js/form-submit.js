$(document).ready(function(){
		$('#form-submit').click(function(event){

			event.preventDefault()

			var name = $('#contact-name').val();
			var email = $('#contact-email').val();
			var subject = $('#contact-subject').val();
			var message = $('#contact-textarea').val();
		
			// Do some extra validation here
			if(email == '' || message == '')
			{
				$('#response').html('<div class="alert alert-danger" role="alert">Please fill out the required fields.</div></span>');
			}
			else
			{
				$.ajax({
					url:"php/send.php",
					method:"POST",
					data:$('#contactForm').serialize(),
					beforeSend:function(){
						$('#response').html('<span class="text-primary">Sending Message...</span>');
					},
					success:function(data){
						$('#contactForm').trigger("reset");
						$('#response').html('<div class="alert alert-success" role="alert">Message Sent Successfully!</div></span>');
					}
				});
			}

		});
	});