	<!-- Header -->
	<?php require_once('header.php'); ?>
<body>
	<div id="topBorder"></div>

	<div id="coverContainer">
		<div id="coverImg">
			<div class="coverTextbox">
				<h1>Welcome to Hálsaból</h1>
				<h3>Vacation cottages at the base of Kirkjufell</h3>
			</div>
		</div>
	</div><!-- coverContainer End -->

	<!-- Navigation Bar -->
	<?php require_once('navbar.php'); ?>
	
	<div class="mainContainer">
		<main>
			<div class="contentText centerblock">

				<h1 class="centerblock">Hálsaból</h1>
				<h2 class="centerblock">Accommodation in Snæfellsnes</h2>
				
				<article>
					<p>
						The Hálsaból cabins are located at the very base of Kirkjufell, the most photographed mountain in Iceland, and the 7th
						most photographed mountain in the world according to <a href="https://www.thephotoargus.com/most-famous-mountains-in-the-world-to-photograph/">thephotoargus.com</a>.
						Kirkjufell is considered to be one of the most famous mountains on earth and has been featured in feature films and television series such as "Game of Thrones" and
						"The Secret Life of Walter Mitty". Kirkjufell is located by the north-center coast of the Snæfellsnes peninsula near the small town of
						Grundarfjörður, and you have the chance to make the very base of it your home right here at Hálsaból while you visit the
						beautiful peninsula of Snæfellsnes.
					</p>
				</article>

				<article>
					<h3>About The Cabins</h3>

					<a class="fancybox" href="assets/images/squareImg1.jpg" data-fancybox-group="gallery" title="From the front porch">
					<img class="thumbImg contentImageRight" src="assets/images/thumb1.jpg" alt="View from from porch" /> </a>

					<p>There are two identical cabins available, each 45 m<sup>2</sup> in size and can comfortably house up to 6 people at a time.
						Each of the cabins consist of 2 identical bedrooms with bunk beds, bottom bunk having double and top bunk having single capacities.
						In addition to that, the living room has a couch which can be retracted and made into a bed for 2 additional guests.</p>
					
					<p>The cabins include modern comforts such as a high speed fiber optic internet connection, a hot tub, gas grill, bluetooth
					speaker, TV with a DVD player, and an outside playground for children,
					all surrounded by the breathtaking landscape of west Iceland.</p>
				</article>

				<article>
					<h3>Grundarfjörður</h3>

					<!-- <a class="fancybox" href="assets/images/squareImg6.jpg" data-fancybox-group="gallery" title="Kirkjufellfoss">
					<img class="thumbImg contentImageLeft" src="assets/images/thumb6.jpg" alt="Kirkjufellfoss" /> </a> -->

					<p>While staying with us make sure to stop by the beautiful nearby town of <a href="https://www.grundarfjordur.is/">Grundarfjörður</a> which is only 3 kilometers east
						of Hálsaból cottages. Grundarfjörður is a small fishing town located at about 180 kilometers north-west of the capital of Reykjavík.
						The population of the town is a little above 900 with the main line of industry being fishing.</p>
					<p>There you can book whale watching tours, visit restaurants, shops, and gather information about places
						of interest in the area from the locals.</p>
				</article>

				<article>
					<h3>Kirkjufellfoss Waterfall</h3>

					<a class="fancybox" href="assets/images/squareImg6.jpg" data-fancybox-group="gallery" title="Kirkjufellfoss">
					<img class="thumbImg contentImageRight" src="assets/images/thumb6.jpg" alt="Kirkjufellfoss" /> </a>

					<p>Kirkjufellfoss (Kirkjufell Waterfall) is less than a kilometer away and only a walking distance from the cabins. It is a popular sight seeing location,
						especially during the summer months, and is	oftentimes quite crowded. So while staying at Hálsaból, it is your perfect
						opportunity to avoid the crowds and catch a time when nobody is there, and enjoy this wonder of nature in piece and serenity.</p>
						
					<p>We recommend dropping by in late evening or even at night to avoid the crowds.</p>
				</article>

				<article>
					<h3>Snæfellsnes</h3>

					<p>Snæfellsnes peninsula is known for its magnificent nature, unique mountains and awe inspiring landscapes.
						The summertime is a popular time to visit, as the scenery around the peninsula of Snæfellsnes is vibrant in color. Mountain ranges
						lush with grass surrounding you from one direction and the ocean from the opposite, and steep glacier roads guiding you around the Snæfellsjökul glacier.
						You will find yourself struck by awe from the sceneries that Snæfellsnes offers.</p>

					<a class="fancybox" href="assets/images/squareImg5.jpg" data-fancybox-group="gallery" title="Kirkjufellfoss">
					<img class="thumbImg contentImageRight" src="assets/images/thumb5.jpg" alt="Kirkjufell Aurora Borealis" /> </a>

					<p>The wintertime however, while dark for the largest portion of the day, possesses magic of its own.
						During the months roughly between November and March, with a little bit of luck you might find youself under a
						lively blanket of northern lights with little to no light pollution getting in the way of the view.</p>

					<p>You can find many interesting and amusing activities to indulge in, whether it be hiking, horseback riding, whale watching,
						cruising the islands of Breiðafjörður, or just kicking back and enjoying life while surrounded by awe inspiring nature.</p>

					<p>Information about local entertainment and interesting places to visit in the surrounding area can be found at the cabins.</p>
				</article>
			</div> <!-- contentText End -->
		</main>
	</div> <!-- mainContainer End -->

<!-- Footer -->
<?php require_once('footer.php'); ?>

</body>
</html>